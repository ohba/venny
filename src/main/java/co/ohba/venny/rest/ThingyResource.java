package co.ohba.venny.rest;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import co.ohba.venny.pojo.Thingy;
import co.ohba.venny.pojo.ThingyComparison;
import co.ohba.venny.service.ThingyService;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class ThingyResource {
	
	@Inject private EntityManager em;
	
	@Inject private ThingyService thingyService;
	
	//@GET
	public List<Thingy> getAll(){
		return em.createQuery("SELECT t FROM Thingy t", Thingy.class).getResultList();
	}
	
	@GET
	@Path("{thing1}/vs/{thing2}")
	public ThingyComparison compare(@PathParam("thing1") String thing1Id, @PathParam("thing2") String thing2Id) {
		return thingyService.compare(thing1Id, thing2Id);
	}
	
	
	@GET
	@Path("new/{id}")
	public Thingy newThingy( @PathParam("id") String id){
		Thingy t = new Thingy();
		t.setId(id.toLowerCase());
		
		em.getTransaction().begin();
		em.persist(t);
		em.getTransaction().commit();
		
		return t;
	}

}
