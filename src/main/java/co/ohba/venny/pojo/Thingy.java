package co.ohba.venny.pojo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

import org.eclipse.persistence.nosql.annotations.DataFormatType;
import org.eclipse.persistence.nosql.annotations.Field;
import org.eclipse.persistence.nosql.annotations.NoSql;

@Data @Entity
@NoSql(dataType="Thingy",dataFormat=DataFormatType.MAPPED)
public class Thingy {
	
	@Id @Field(name="_id") 
	private String id;
	
	@ElementCollection
	private Set<String> properties = new HashSet<String>();
	
	public Set<String> getProperties() {
		return properties==null? new HashSet<String>() : properties;
	}
	
	public void setProperties(Set<String> props) {
		properties = props==null? new HashSet<String>() : props;
	}

}
