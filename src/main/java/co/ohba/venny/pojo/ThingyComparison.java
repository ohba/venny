package co.ohba.venny.pojo;

import java.util.Set;

import lombok.Data;

@Data
public class ThingyComparison {
	
	private String thing1;
	private String thing2;
	private Set<String> common;
	private Set<String> uniq1;
	private Set<String> uniq2;

}
