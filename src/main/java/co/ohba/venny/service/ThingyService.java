package co.ohba.venny.service;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import lombok.val;
import co.ohba.venny.pojo.Thingy;
import co.ohba.venny.pojo.ThingyComparison;

public class ThingyService {
	
	@Inject private EntityManager em;

	public ThingyComparison compare(String thing1Id, String thing2Id) {
		
		Thingy thing1 = em.find(Thingy.class, thing1Id),
				thing2 = em.find(Thingy.class, thing2Id);
		
		thing1 = thing1==null?new Thingy():thing1;
		thing2 = thing2==null?new Thingy():thing2;
				
		val compare = new ThingyComparison();
		compare.setThing1(thing1.getId());
		compare.setThing2(thing2.getId());
		
		Set<String> t1Props = thing1.getProperties(),
					t2Props = thing2.getProperties(),
					common = new HashSet<String>();
		
		for(String prop : t1Props.toArray(new String[] {})) {
			if(t2Props.contains(prop)) {
				t1Props.remove(prop);
				t2Props.remove(prop);
				common.add(prop);
			}
		}
		
		compare.setUniq1(t1Props);
		compare.setUniq2(t2Props);
		compare.setCommon(common);
		
		return compare;
	}

}
